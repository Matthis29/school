<?php
function init_Connection(){
	$connect = mysqli_connect("localhost","root","","boutique");

	if (mysqli_connect_errno())
	{
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	mysqli_set_charset($connect, "utf-8");

	return $connect;
}

function getAllGames($connect){
	$q = "SELECT `id`, `titre`, `genre`, `desCourte`, `desLong` FROM `jeux` WHERE 1";

	$gameQuery = mysqli_query($connect, $q);

	$arrayGame = array();

	while($ligne = mysqli_fetch_assoc($gameQuery)){

		$game = new Jeu;
		$game->id = $ligne["id"];
		$game->titre = $ligne["titre"];
		$game->genre = $ligne["genre"];
		$game->desCourte = $ligne["desCourte"];
		$game->desLong = $ligne["desLong"];
		$arrayGame[] = $game;

	}

	return $arrayGame;
}

function getGameByID($connect, $id){
	$q = "SELECT `id`, `titre`, `genre`, `desCourte`, `desLong` FROM `jeux` WHERE id='$id'";

	$gameQuery = mysqli_query($connect, $q);

	$ligne = mysqli_fetch_assoc($gameQuery);

	$game = new Jeu;
	$game->id = $ligne["id"];
	$game->titre = $ligne["titre"];
	$game->genre = $ligne["genre"];
	$game->desCourte = $ligne["desCourte"];
	$game->desLong = $ligne["desLong"];

	return $game;
}

function requestFormAndUpdate($connect, $id){
	$titre = $_REQUEST["titre"];
	if (isset($_REQUEST["category"]) && $_REQUEST["existingValue"] == "") {
		$category = $_REQUEST["category"];
	} else {
		$category = $_REQUEST["existingValue"];
	}

	$desLong = $_REQUEST["description-longue"];
	$desCourte = $_REQUEST["description-courte"];

	$updateRequest = "UPDATE `jeux` SET `titre`='$titre',`genre`='$category',`desCourte`='$desCourte',`desLong`='$desLong' WHERE id=$id";
	$updateQuery = mysqli_query($connect, $updateRequest);
}

function requestFormAndInser($connect) {
	$titre = $_REQUEST["titre"];
	if (isset($_REQUEST["category"]) && $_REQUEST["existingValue"] == "") {
		$category = $_REQUEST["category"];
	} else {
		$category = $_REQUEST["existingValue"];
	}

	$desLong = $_REQUEST["description-longue"];
	$desCourte = $_REQUEST["description-courte"];

	$insertRequest = "INSERT INTO `jeux`(`titre`, `genre`, `desCourte`, `desLong`) VALUES ('$titre','$category','$desCourte','$desLong')";
	if (mysqli_query($connect, $insertRequest)) {
	     mysqli_insert_id($connect);
	}
}

?>