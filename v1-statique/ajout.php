<!DOCTYPE html>
<html>
<head>
	<title>Ajout - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Ajouter un jeux</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<form method="GET" action="ajout.php">
				<p><input type="texte" name="name" placeholder="Nom du jeux"></p>
				<p><input type="texte" name="category" placeholder="Catégorie du jeux">
					<select>
						<option></option>
						<option>MOBA</option>
						<option>FPS</option>
					</select>
				</p>
				<p><textarea name="description-courte" placeholder="Description courte du jeux"></textarea></p>
				<p><textarea name="description-longue" placeholder="Description longue du jeux"></textarea></p>
				<p><input type="submit" value="Ajouter"></p>
				
			</form>
		</article>
	</section>
</body>
</html>