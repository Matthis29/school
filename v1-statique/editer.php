<!DOCTYPE html>
<html>
<head>
	<title>Edition de LOL - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Editer LOL</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<form method="GET" action="editer.php">
				<p>
					<label for="titre">Titre :</label>
					<input type="texte" name="name" value="LOL" id="titre">
				</p>
				<p>
					<label for="category">Catégorie :</label>
					<input type="texte" name="category" value="MOBA" id="category">
					<select>
						<option></option>
						<option selected>MOBA</option>
						<option>FPS</option>
					</select>
				</p>
				<p><label for="description-longue">Description longue :</label>
					<textarea name="description-courte" id="description-longue">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor.</textarea></p>

				<p>
					<label for="description-courte">Description longue :</label>
					<textarea name="description-longue" id="description-courte">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea></p>
				<p><input type="submit" value="Editer"></p>
				
			</form>
		</article>
	</section>
</body>
</html>