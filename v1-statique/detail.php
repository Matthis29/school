<!DOCTYPE html>
<html>
<head>
	<title>LOL - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>LOL</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<h3>LOL</h3>
			<p class="category">MOBA</p>
			<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<ul class="actions">
				<li><a href="editer.php">Editer</a></li>
			</ul>
			
		</article>
	</section>
</body>
</html>