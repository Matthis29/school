<!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Tous les jeux</h2>
	</header>
	<nav>
		<ul>
			<p>Filtrer par genre</p>
			<li><a href="">MOBA</a></li>
			<li><a href="">FPS</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<h3>LOL</h3>
			<p class="category">MOBA</p>
			<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor.</p>
			<ul class="actions">
				<li><a href="detail.php">Detail</a></li>
				<li><a href="editer.php">Editer</a></li>
			</ul>
			
		</article>

		<article>
			<h3>COD</h3>
			<p class="category">FPS</p>
			<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor.</p>
			<ul class="actions">
				<li><a href="">Detail</a></li>
				<li><a href="">Editer</a></li>
			</ul>
			
		</article>
	</section>
	<footer>
		&copy SUPER BOUTIQUE
		<p><a href="ajout.php">Ajouter un jeux</a></p>
	</footer>
</body>
</html>