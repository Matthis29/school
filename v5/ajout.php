<?php

include 'Genre.php';
include 'functions.php';

$connect = init_Connection();

$arrayGenre = getAllGenre($connect);


if(isset($_REQUEST["nbmSubmit"])){

	requestFormAndInser($connect);
	header('Location: index.php');
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Edition de LOL - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Editer LOL</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<form method="POST">

				<p>
					<label for="titre">Titre :</label>
					<input type="texte" name="titre" id="titre">
				</p>
				<p>
					<label for="category">Catégorie :</label>
					<input type="texte" name="category" id="category">
					<select name="existingValue">
						<option selected></option>
						<?php
							foreach ($arrayGenre as $genre) { ?>
								<option value="<?php echo $genre->id; ?>"><?php echo $genre->genre; ?></option>
							<?php }
						?>
					</select>
				</p>
				<p><label for="description-longue">Description longue :</label>
					<textarea name="description-courte" id="description-longue"></textarea></p>

				<p>
					<label for="description-courte">Description longue :</label>
					<textarea name="description-longue" id="description-courte"></textarea></p>
				<p><input type="submit" name="nbmSubmit" value="Editer"></p>
				
			</form>
		</article>
	</section>
</body>
</html>