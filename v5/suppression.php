<?php

include 'functions.php';

$connect = init_Connection();

if (isset($_REQUEST["id"])) {
	$id = $_REQUEST["id"];

	deleteGame($connect, $id);
	header('Location: index.php');

} else {
	header('Location: index.php');
}
?>