<?php
$connect = mysqli_connect("localhost","root","","boutique");

if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
mysqli_set_charset($connect, "utf-8");

if(isset($_REQUEST["nbmSubmit"])){
	$titre = $_REQUEST["titre"];
	if (isset($_REQUEST["category"]) && $_REQUEST["existingValue"] == "") {
		$category = $_REQUEST["category"];
	} else {
		$category = $_REQUEST["existingValue"];
	}

	$desLong = $_REQUEST["description-longue"];
	$desCourte = $_REQUEST["description-courte"];

	$insertRequest = "INSERT INTO `jeux`(`titre`, `genre`, `desCourte`, `desLong`) VALUES ('$titre','$category','$desCourte','$desLong')";
	if (mysqli_query($connect, $insertRequest)) {
	     mysqli_insert_id($connect);
	}
	header('Location: index.php');
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Edition de LOL - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Editer LOL</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<form method="POST">

				<p>
					<label for="titre">Titre :</label>
					<input type="texte" name="titre" id="titre">
				</p>
				<p>
					<label for="category">Catégorie :</label>
					<input type="texte" name="category" id="category">
					<select name="existingValue">
						<option selected></option>
						<option >MOBA</option>
						<option>FPS</option>
					</select>
				</p>
				<p><label for="description-longue">Description longue :</label>
					<textarea name="description-courte" id="description-longue"></textarea></p>

				<p>
					<label for="description-courte">Description longue :</label>
					<textarea name="description-longue" id="description-courte"></textarea></p>
				<p><input type="submit" name="nbmSubmit" value="Editer"></p>
				
			</form>
		</article>
	</section>
</body>
</html>