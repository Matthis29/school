<?php

$connect = mysqli_connect("localhost","root","","boutique");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
mysqli_set_charset($connect, "utf-8");

$q = "SELECT * FROM `jeux` WHERE 1";

$gameQuery = mysqli_query($connect, $q);

$arrayGame = array();

while($ligne = mysqli_fetch_assoc($gameQuery)){

	$arrayGame[] = $ligne;
}

print_r($arrayGame);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Tous les jeux</h2>
	</header>
	<nav>
		<ul>
			<p>Filtrer par genre</p>
			<li><a href="">MOBA</a></li>
			<li><a href="">FPS</a></li>
		</ul>
	</nav>
	<section>
		<article>
			<?php
				foreach ($arrayGame as $game) { ?>
				<h3><?php echo $game["titre"]; ?></h3>
				<p class="category"><?php echo $game["genre"]; ?></p>
				<p class="description"><?php echo $game["desCourte"]; ?></p>
				<ul class="actions">
					<li><a href="detail.php?id=<?php echo $game["id"]; ?>">Detail</a></li>
					<li><a href="editer.php?id=<?php echo $game["id"]; ?>">Editer</a></li>
				</ul>					
				<?php }
			?>
			
		</article>
	</section>
	<footer>
		&copy SUPER BOUTIQUE
		<p><a href="ajout.php">Ajouter un jeux</a></p>
	</footer>
</body>
</html>