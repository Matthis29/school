<?php

include 'DaoSQLGame.php';

$dao = new DaoSQLGame();

if (isset($_REQUEST["id"])) {
	$id = $_REQUEST["id"];

	$dao->deleteGame($id);
	header('Location: index.php');

} else {
	header('Location: index.php');
}
?>