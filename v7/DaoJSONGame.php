<?php
class DaoJSONGame{
	private $jeux;
	private $category;

	function __construct(){

		$this->jeux = file_get_contents('jeux.json');
		$this->category = file_get_contents('category.json');
	}

	public function getGenre($id){
		foreach (json_decode($this->category) as $genre) {
			if ($genre->genreId == $id) {
				$nomGenre = $genre->genre;	

				return $nomGenre;							
			}
			
		}
	}

	public function getAllGames(){


		$arrayGame = array();

		foreach (json_decode($this->jeux) as $jeux) {

			$game = new Jeu;
			$game->id = $jeux->id;
			$game->titre = $jeux->titre;

			$id = $jeux->idGenre;
			$nomGenre = $this->getGenre($id);

			$game->genre = $jeux->idGenre;

			$game->genre = $nomGenre;
			$game->desCourte = $jeux->desCourte;
			$game->desLong = $jeux->desLong;
			$arrayGame[] = $game;
		}

		return $arrayGame;
	}

	public function getGameByID($id){
		foreach (json_decode($this->jeux) as $jeu) {
			if ($jeu->id == $id) {
				$game = new Jeu;
				$game->id = $jeu->id;
				$game->titre = $jeu->titre;
				$id = $jeu->idGenre;
				$nomGenre = $this->getGenre($id);
				$game->genre = $nomGenre;
				$game->desCourte = $jeu->desCourte;
				$game->desLong = $jeu->desLong;	

				return $game;					
			}
			
		}
	}

	public function getAllGenre(){
		$arrayGenre = array();
		foreach (json_decode($this->category) as $genres) {
			$genre = new Genre;
			$genre->id = $genres->genreId;
			$genre->genre = $genres->genre;
			$arrayGenre[] = $genre;
			
		}

		return $arrayGenre;

	}

	function requestFormAndUpdate($id){
		$titre = $_REQUEST["titre"];
		if (isset($_REQUEST["newGenre"]) && $_REQUEST["existingValue"] == "") {
			$idNewCategory = $_REQUEST["newGenre"];

			echo "ok";
			$newId = $this->insertNewGenre($idNewCategory);
			print_r($newId);

			$category = $newId;		
		} else {
			$category = $_REQUEST["existingValue"];
		}

		$desLong = $_REQUEST["description-longue"];
		$desCourte = $_REQUEST["description-courte"];

		$jsonGameArray = json_decode($this->jeux);
		foreach ($jsonGameArray as $i => $jsonGame) {
			if ($jsonGame->id == $id) {
				$game = new Jeu;
				$game->id = $id;
				$game->titre = $titre;
				$game->idGenre = $category;
				$game->desCourte = $desCourte;
				$game->desLong = $desLong;
				$jsonGameArray[$i] = $game;
				$content = json_encode($jsonGameArray);
				$file = fopen('jeux.json', 'w+');
				if ($content != null){
					fwrite($file, $content);
					fclose($file);
				}
			}
		}
	}

	public function insertNewGenre($newGenre){
		$category = json_decode($this->category);

		$categ = new Genre;
		$categ->genreId = count($category) + 1;
		$categ->genre = $newGenre;
		$category[] = $categ;
		$content = json_encode($category);
		$file = fopen('category.json', 'w+');
		if ($content != null){
			fwrite($file, $content);
			fclose($file);
		}

		return $categ->genreId;
	}

	function requestFormAndInser() {
		$titre = $_REQUEST["titre"];
		if (isset($_REQUEST["newGenre"]) && $_REQUEST["existingValue"] == "") {
			$idNewCategory = $_REQUEST["newGenre"];

			echo "ok";
			$newId = $this->insertNewGenre($idNewCategory);
			print_r($newId);

			$category = $newId;		
		} else {
			$category = $_REQUEST["existingValue"];
		}

		$desLong = $_REQUEST["description-longue"];
		$desCourte = $_REQUEST["description-courte"];

		$jsonGameArray = json_decode($this->jeux);
		$game = new Jeu;
		$game->id = count($jsonGameArray) + 1;;
		$game->titre = $titre;
		$game->idGenre = $category;
		$game->desCourte = $desCourte;
		$game->desLong = $desLong;
		$jsonGameArray[] = $game;
		$content = json_encode($jsonGameArray);
		$file = fopen('jeux.json', 'w+');
		if ($content != null){
			fwrite($file, $content);
			fclose($file);
		}


	}

	function deleteGame($id){
	}
}
?>