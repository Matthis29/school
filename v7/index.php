<?php


include 'Jeu.php';
include 'DaoSQLGame.php';
include 'DaoJSONGame.php';
include 'ChooseDAO.php';
include 'functions.php';

session_start();



$chooseDAO = new ChooseDAO;
$dao = $chooseDAO->getDAO();
$arrayGame = $dao->getAllGames();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>Tous les jeux</h2>
	</header>
	<nav class="menuGame">
			<form>
				<button value="sql" name="daovalue">SQL</button>
				<button value="json" name="daovalue">JSON</button>
			</form>
	</nav>
	<section class="sectionGames">
		
			<?php
				foreach ($arrayGame as $game) { ?>
					<article class="games">
						<h3><?php echo $game->titre; ?></h3>
						<p class="category"><?php echo $game->genre; ?></p>
						<p class="description"><?php echo $game->desCourte; ?></p>
						<ul class="actions">
							<li><a href="detail.php?id=<?php echo $game->id; ?>">Detail</a></li>
							<li><a href="editer.php?id=<?php echo $game->id; ?>">Editer</a></li>
							<li><a href="suppression.php?id=<?php echo $game->id; ?>">Supprimer</a></li>
						</ul>	
					</article>				
				<?php }
			?>
			

	</section>
	<footer>
		&copy SUPER BOUTIQUE
		<p><a href="ajout.php">Ajouter un jeux</a></p>
	</footer>
</body>
</html>