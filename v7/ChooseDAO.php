<?php
class ChooseDAO {
	private $sql;
	private $json;
	private $currentDAO;

	function __construct(){
		$this->mysql = new DaoSQLGame;
		$this->json = new DaoJSONGame;
		$this->currentDAO = $this->mysql;

	}

	public function getDAO(){

		$this->tcheckDAO();

		return $this->currentDAO;

	}

	private function tcheckDAO(){
		if (isset($_SESSION["dao"])) {
			$this->currentDAO = $this->getDaoType($_SESSION["dao"]);
		}

		if (isset($_REQUEST["daovalue"])) {
			$this->currentDAO = $this->getDaoType($_REQUEST["daovalue"]);
			$_SESSION["dao"] = $this->currentDAO;
		}
	}

	private function getDaoType($type){
		switch($type):
			case 'sql':
				return $this->mysql;
			case 'json':
				return $this->json;
			default:
				return $this->mysql;
		endSwitch;
	}
}
?>