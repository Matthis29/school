<?php
function init_Connection(){
	$connect = mysqli_connect("localhost","root","","boutique");

	if (mysqli_connect_errno())
	{
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	mysqli_set_charset($connect, "utf-8");

	return $connect;
}

function getAllGames($connect){
	$q = "SELECT `id`, `titre`, jeux.genre, `idGenre`, `desCourte`, `desLong`, category.genre as genres FROM `jeux` LEFT JOIN category ON jeux.idGenre = category.genreId WHERE 1";

	$gameQuery = mysqli_query($connect, $q);

	$arrayGame = array();

	while($ligne = mysqli_fetch_assoc($gameQuery)){

		$game = new Jeu;
		$game->id = $ligne["id"];
		$game->titre = $ligne["titre"];
		$game->genre = $ligne["genres"];
		$game->desCourte = $ligne["desCourte"];
		$game->desLong = $ligne["desLong"];
		$arrayGame[] = $game;

	}

	return $arrayGame;
}

function getAllGenre($connect){
	$q = "SELECT genreId, genre FROM category WHERE 1";

	$query = mysqli_query($connect, $q);

	$arrayGenre = array();

	while($ligne = mysqli_fetch_assoc($query)){
		$genre = new Genre;
		$genre->id = $ligne["genreId"];
		$genre->genre = $ligne["genre"];
		$arrayGenre[] = $genre;

	}

	return $arrayGenre;
}

function getGameByID($connect, $id){
	$q = "SELECT `id`, `titre`, jeux.genre, `idGenre`, `desCourte`, `desLong`, category.genre FROM `jeux` LEFT JOIN category ON jeux.idGenre = category.genreId WHERE id='$id'";

	$gameQuery = mysqli_query($connect, $q);

	$ligne = mysqli_fetch_assoc($gameQuery);

	$game = new Jeu;
	$game->id = $ligne["id"];
	$game->titre = $ligne["titre"];
	$game->genre = $ligne["genre"];
	$game->desCourte = $ligne["desCourte"];
	$game->desLong = $ligne["desLong"];

	return $game;
}

function requestFormAndUpdate($connect, $id){
	$titre = $_REQUEST["titre"];
	if (isset($_REQUEST["newGenre"]) && $_REQUEST["existingValue"] == "") {
		$idNewCategory = $_REQUEST["newGenre"];

		$newId = insertNewGenre($connect, $idNewCategory);

		$category = $newId;		
	} else {
		$category = $_REQUEST["existingValue"];
	}

	$desLong = $_REQUEST["description-longue"];
	$desCourte = $_REQUEST["description-courte"];

	$updateRequest = "UPDATE `jeux` SET `titre`='$titre',`idGenre`='$category',`desCourte`='$desCourte',`desLong`='$desLong' WHERE id=$id";
	$updateQuery = mysqli_query($connect, $updateRequest);
}

function insertNewGenre($connect, $newGenre){
	$insertGenre = "INSERT INTO `category`(`genre`) VALUES ('$newGenre')";
	echo $insertGenre;
	if (mysqli_query($connect, $insertGenre)) {
	     $newId = mysqli_insert_id($connect);

	     return $newId;
	}

	return false;
}

function requestFormAndInser($connect) {
	$titre = $_REQUEST["titre"];
	if (isset($_REQUEST["newGenre"]) && $_REQUEST["existingValue"] == "") {
		$idNewCategory = $_REQUEST["newGenre"];

		$newId = insertNewGenre($connect, $idNewCategory);

		$category = $newId;		
	} else {
		$category = $_REQUEST["existingValue"];
	}

	$desLong = $_REQUEST["description-longue"];
	$desCourte = $_REQUEST["description-courte"];

	$insertRequest = "INSERT INTO `jeux`(`titre`, `idGenre`, `desCourte`, `desLong`) VALUES ('$titre','$category','$desCourte','$desLong')";
	echo $insertRequest;
	if (mysqli_query($connect, $insertRequest)) {
	     mysqli_insert_id($connect);
	}
}

function deleteGame($connect, $id){
	$q = "DELETE FROM `jeux` WHERE id=$id";

	mysqli_query($connect, $q);
}

?>