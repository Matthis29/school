<?php

include 'Jeu.php';
include 'DaoSQLGame.php';
include 'functions.php';

$dao = new DaoSQLGame();

if (isset($_REQUEST["id"])) {
	$id = $_REQUEST["id"];

	$arrayGameById = $dao->getGameByID($id);

} else {
	header('Location: index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>LOL - Boutique</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
		<h1>Catalogue jeux</h1>
		<h2>LOL</h2>
	</header>
	<nav>
		<ul>
			<li><a href="index.php">Retour</a></li>
		</ul>
	</nav>
	<section>
		<article>
			
			<h3><?php echo $arrayGameById->titre; ?></h3>
			<p class="category"><?php echo $arrayGameById->genre; ?></p>
			<p class="description"><?php echo $arrayGameById->desLong; ?></p>
			<ul class="actions">
				<li><a href="editer.php?id=<?php echo $arrayGameById->id; ?>">Editer</a></li>
			</ul>					
			
		</article>
	</section>
</body>
</html>